/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sumerge.program.rest;

import com.sumerge.program.user.entity.User;
import com.sumerge.program.user.entity.UserManager;
import static java.util.logging.Level.SEVERE;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.ws.rs.*;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import javax.ws.rs.core.Response;

@RequestScoped
@Produces(APPLICATION_JSON)
@Consumes(APPLICATION_JSON)
@Path("UsersProfile")
public class UserResources {
    private static UserManager users  = new UserManager();
    private static final Logger LOGGER = Logger.getLogger(UserResources.class.getName());
    
    @GET
    public Response getAllUsers(){
        try{
            return Response.ok().
                    entity(users.getAllUsers()).
                    build();
            
        }catch(Exception e){
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();

        }
    }
    
    @GET
    @Path("{id}")
    public Response getUserByID(@PathParam("id") long id){
        try{
            return Response.ok().
                    entity(users.getUserByID(id)).
                    build();
            
        }catch(Exception e){
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    
    @GET
    @Path("UserByName/{name}")
    public Response getUserByName(@PathParam("name") String name){
        try{
            return Response.ok().
                    entity(users.getUserByName(name)).
                    build();
            
        }catch(Exception e){
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    
    @GET
    @Path("UserByEmail/{email}")
    public Response getUserByEmail(@PathParam("email") String email){
        try{
            return Response.ok().
                    entity(users.getUserByEmail(email)).
                    build();
            
        }catch(Exception e){
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    
    @GET
    @Path("UserByAddress/{address}")
    public Response getUserByAddress(@PathParam("address") String address){
        try{
            return Response.ok().
                    entity(users.getUserByAddress(address)).
                    build();
            
        }catch(Exception e){
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    
    @DELETE
    @Path("{id}")
    public Response deleteUser(@PathParam("id") long id){        
        try{
            users.deleteUserByID(id);
            return Response.ok().
                    build();
            
        }catch(Exception e){
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();

        }
    }
    
    @PUT
    public Response editUser(User user) {
        try {            
            if(!users.editUserByID(user))
                throw new IllegalArgumentException("Can't edit user: invalid ID");
            return Response.ok().
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }

    @POST
    public Response addUser(User user) {
        try {
            return Response.ok().
                    entity(users.addUser(user)).
                    build();
        } catch (Exception e) {
            LOGGER.log(SEVERE, e.getMessage(), e);
            return Response.serverError().
                    entity(e.getClass() + ": " + e.getMessage()).
                    build();
        }
    }
    
}
